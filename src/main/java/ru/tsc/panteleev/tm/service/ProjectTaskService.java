package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.service.IProjectTaskService;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.panteleev.tm.exception.field.TaskIdEmptyException;
import ru.tsc.panteleev.tm.model.Task;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @Nullable
    private final IProjectRepository projectRepository;

    @Nullable
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@Nullable final IProjectRepository projectRepository, @Nullable final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty())throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = Optional.ofNullable(taskRepository.findById(userId, taskId))
                            .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = Optional.ofNullable(taskRepository.findById(userId, taskId))
                            .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        tasks.stream().forEach(task -> taskRepository.removeById(userId, task.getId()));
        projectRepository.removeById(userId, projectId);
    }

}
