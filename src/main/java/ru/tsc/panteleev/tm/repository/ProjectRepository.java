package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

}
